struct param {
	int borne_inf;
	int borne_sup;
};

program TDM4 {
	version VERSION {
		int SOMME(param) = 1;
	} = 1;
} = 0x20000001;