#include <stdio.h>

void saisirEntier(int *a, int* b) {
	printf("Veuillez saisir le premier entier: \n");
	scanf("%d", a);
	printf("Veuillez saisir le second entier: \n");
	scanf("%d", b);
}

int main(int argc, char *argv[]) {
	int a, b;

	saisirEntier(&a, &b);

	printf("Somme: %d + %d = %d\n", a, b, a+b);
}