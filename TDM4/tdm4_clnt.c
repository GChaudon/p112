/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#include <memory.h> /* for memset */
#include "tdm4.h"

/* Default timeout can be changed using clnt_control() */
static struct timeval TIMEOUT = { 25, 0 };

int *
somme_1(param *argp, bool_t (* each_result)())
{
	static int clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_broadcast (TDM4, VERSION, SOMME,
		(xdrproc_t) xdr_param, (caddr_t) argp,
		(xdrproc_t) xdr_int, (caddr_t) &clnt_res,
		each_result) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

