/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include "tdm4.h"

#include <stdio.h>
#include <string.h>

void saisirEntier(param *p) {
	printf("Veuillez saisir le premier entier: \n");
	scanf("%d", &p->borne_inf);
	printf("Veuillez saisir le second entier: \n");
	scanf("%d", &p->borne_sup);
}

int eachResult(char *resultp, struct sockaddr_in *addr) {
	printf("Résultat: %d\n", *((int *)resultp));

	return 0;
}

int main (int argc, char *argv[]) {
	CLIENT *clnt;
	int *res;
	param p;

	if(argc != 2) {
		printf("Usage: %s <mode>\n", argv[0]);
		return 1;
	}

	saisirEntier(&p);

	if(!strcmp(argv[1], "unicast")) {
		char *machines[] = {"localhost", "voydstack.fr", NULL};
		int i = 0;
		while(machines[i]) {
			clnt = clnt_create (machines[i], TDM4, VERSION, "udp");
			res = somme_1(&p, clnt);
			printf("[%s] Résultat: %d\n", machines[i], *res);
			i++;
		}
	} else if(!strcmp(argv[1], "multicast")) {
		somme_1(&p, &eachResult);

		printf("Résultat: %d\n", *res);
	} else {
		printf("Mode non reconnu\n");
	}

	exit(0);
}
