/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include "tdm4.h"

int *
somme_1_svc(param *p, struct svc_req *rqstp)
{
	static int  result;

	int a = p->borne_inf;
	int b = p->borne_sup;
	int somme = a + b;

	printf("Somme: %d + %d = %d\n", a, b, somme);

	result = somme;

	return &result;
}
