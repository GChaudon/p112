/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include "tdm5.h"

liste *
ls_1_svc(filename *argp, struct svc_req *rqstp)
{
	static liste  result;

	/*
	 * insert server code here
	 */

	return &result;
}

liste *
readfile_1_svc(filename *argp, struct svc_req *rqstp)
{
	static liste  result;

	/*
	 * insert server code here
	 */

	return &result;
}

int *
writefile_1_svc(params_write *argp, struct svc_req *rqstp)
{
	static int  result;

	/*
	 * insert server code here
	 */

	return &result;
}
