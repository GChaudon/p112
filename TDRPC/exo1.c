#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	if(argc != 2) {
		printf("Usage: %s <message>", argv[0]);
		exit(0);
	}

	FILE *null = fopen("/dev/null", "w");
	fprintf(null, "%s", argv[1]);
	fclose(null);

	return 0;
}